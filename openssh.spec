%global gtk2 1
%global pie 1

# Add option to build without GTK2 for older platforms with only GTK+.
# rpm -ba|--rebuild --define 'no_gtk2 1'
%{?no_gtk2:%global gtk2 0}

%global sshd_uid    74
%global openssh_release 5

Name:           openssh
Version:        9.6p1
Release:        %{openssh_release}
%global openssh_version %{version}
URL:            http://www.openssh.com/portable.html
License:        BSD
Summary:        An open source implementation of SSH protocol version 2

Source0:        https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-%{version}.tar.gz
Source1:        https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-%{version}.tar.gz.asc
Source2:        sshd.pam
Source3:        http://prdownloads.sourceforge.net/pamsshagentauth/pam_ssh_agent_auth/pam_ssh_agent_auth-0.10.4.tar.gz
Source4:        pam_ssh_agent-rmheaders
Source5:        ssh-keycat.pam
Source6:        sshd.sysconfig
Source7:        sshd@.service
Source8:        sshd.socket
Source9:        sshd.service
Source10:       sshd-keygen@.service
Source11:       sshd-keygen
Source12:       sshd.tmpfiles
Source13:       sshd-keygen.target
Source14:       ssh-agent.service
Source15:       ssh-agent.socket
Source16:       ssh-keygen-bash-completion.sh
Source17:       ssh-host-keys-migration.sh
Source18:       ssh-host-keys-migration.service
Patch0:         openssh-6.7p1-coverity.patch
Patch1:         openssh-7.6p1-audit.patch
Patch2:         openssh-7.1p2-audit-race-condition.patch
Patch3:         openssh-9.0p1-audit-log.patch
Patch4:         pam_ssh_agent_auth-0.9.3-build.patch
Patch5:         pam_ssh_agent_auth-0.10.3-seteuid.patch
Patch6:         pam_ssh_agent_auth-0.9.2-visibility.patch
Patch7:         pam_ssh_agent_auth-0.9.3-agent_structure.patch
Patch8:         pam_ssh_agent_auth-0.10.2-compat.patch
Patch9:         pam_ssh_agent_auth-0.10.2-dereference.patch
Patch10:        pam_ssh_agent_auth-0.10.4-rsasha2.patch
Patch11:        pam_ssh_agent-configure-c99.patch
Patch12:        openssh-7.8p1-role-mls.patch
Patch13:        openssh-6.6p1-privsep-selinux.patch
Patch14:        openssh-6.6p1-keycat.patch
Patch15:        openssh-6.6p1-allow-ip-opts.patch
Patch16:        openssh-5.9p1-ipv6man.patch
Patch17:        openssh-5.8p2-sigpipe.patch
Patch18:        openssh-7.2p2-x11.patch
Patch19:        openssh-7.7p1-fips.patch
Patch20:        openssh-5.1p1-askpass-progress.patch
Patch21:        openssh-4.3p2-askpass-grab-info.patch
Patch22:        openssh-7.7p1.patch
Patch23:        openssh-7.8p1-UsePAM-warning.patch
Patch24:        openssh-8.0p1-gssapi-keyex.patch
Patch25:        openssh-6.6p1-force_krb.patch
Patch26:        openssh-6.6p1-GSSAPIEnablek5users.patch
Patch27:        openssh-7.7p1-gssapi-new-unique.patch
Patch28:        openssh-7.2p2-k5login_directory.patch
Patch29:        openssh-6.6p1-kuserok.patch
Patch30:        openssh-6.4p1-fromto-remote.patch
Patch31:        openssh-6.6.1p1-selinux-contexts.patch
Patch32:        openssh-6.6.1p1-log-in-chroot.patch
Patch33:        openssh-6.6.1p1-scp-non-existing-directory.patch
Patch34:        openssh-6.8p1-sshdT-output.patch
Patch35:        openssh-6.7p1-sftp-force-permission.patch
Patch36:        openssh-7.2p2-s390-closefrom.patch
Patch37:        openssh-7.3p1-x11-max-displays.patch
Patch38:        openssh-7.4p1-systemd.patch
Patch39:        openssh-7.6p1-cleanup-selinux.patch
Patch40:        openssh-7.5p1-sandbox.patch
Patch41:        openssh-8.0p1-pkcs11-uri.patch
Patch42:        openssh-7.8p1-scp-ipv6.patch
Patch43:        openssh-8.0p1-crypto-policies.patch
Patch44:        openssh-9.3p1-merged-openssl-evp.patch
Patch45:        openssh-8.0p1-openssl-kdf.patch
Patch46:        openssh-8.2p1-visibility.patch
Patch47:        openssh-8.2p1-x11-without-ipv6.patch
Patch48:        openssh-8.0p1-keygen-strip-doseol.patch
Patch49:        openssh-8.0p1-preserve-pam-errors.patch
Patch50:        openssh-8.7p1-scp-kill-switch.patch
Patch51:        openssh-8.7p1-recursive-scp.patch
Patch52:        openssh-8.7p1-minrsabits.patch
Patch53:        openssh-8.7p1-ibmca.patch
Patch54:        openssh-8.7p1-ssh-manpage.patch
Patch55:        openssh-8.7p1-negotiate-supported-algs.patch

Patch56:        bugfix-sftp-when-parse_user_host_path-empty-path-should-be-allowed.patch
Patch57:        bugfix-openssh-add-option-check-username-splash.patch
Patch58:        feature-openssh-7.4-hima-sftpserver-oom-and-fix.patch
Patch59:        bugfix-openssh-fix-sftpserver.patch
Patch60:        set-sshd-config.patch
Patch61:        feature-add-SMx-support.patch
Patch62:        add-loongarch.patch
Patch63:        openssh-Add-sw64-architecture.patch
Patch64:        add-strict-scp-check-for-CVE-2020-15778.patch
Patch65:        skip-scp-test-if-there-is-no-scp-on-remote-path-as-s.patch
Patch66:        set-ssh-config.patch
Patch67:        backport-fix-CVE-2024-6387.patch

Patch68:        backport-upstream-ensure-key_fd-is-filled-when-DSA-is-disable.patch
Patch69:        backport-upstream-Fix-proxy-multiplexing-O-proxy-bug.patch
Patch70:        backport-upstream-make-parsing-user-host-consistently-look-for-the-last-in.patch
Patch71:        backport-upstream-Do-not-apply-authorized_keys-options-when-signature.patch
Patch72:        backport-upstream-some-extra-paranoia.patch
Patch73:        backport-fix-CVE-2024-39894.patch
Patch74:        backport-fix-CVE-2025-26465.patch
Patch75:        backport-fix-CVE-2025-26466.patch

Requires:       /sbin/nologin
Requires:       libselinux >= 2.3-5 audit-libs >= 1.0.8
Requires:       openssh-server = %{version}-%{release}

BuildRequires:  gtk2-devel libX11-devel openldap-devel autoconf automake perl-interpreter perl-generators
BuildRequires:  zlib-devel audit-libs-devel >= 2.0.5 util-linux groff pam-devel dos2unix
BuildRequires:  openssl-devel >= 0.9.8j perl-podlators systemd-devel gcc p11-kit-devel krb5-devel
BuildRequires:  libedit-devel ncurses-devel libselinux-devel >= 2.3-5 audit-libs >= 1.0.8 xauth gnupg2

Recommends:     p11-kit

%package        clients
Summary:        An open source SSH client applications
Requires:       openssh = %{version}-%{release}
Requires:       crypto-policies >= 20180306-1

%package        server
Summary:        An open source SSH server daemon
Requires:       openssh = %{version}-%{release}
Requires(pre):  shadow
Requires:       pam >= 1.0.1-3
Requires:       crypto-policies >= 20180306-1
%{?systemd_requires}

%package        keycat
Summary:        A mls keycat backend for openssh
Requires:       openssh = %{version}-%{release}

%package        askpass
Summary:        A passphrase dialog for OpenSSH and X
Requires:       openssh = %{version}-%{release}

%package -n pam_ssh_agent_auth
Summary:        PAM module for authentication with ssh-agent
Version:        0.10.4
Release:        4.%{openssh_release}
License:        BSD

%description
OpenSSH is the premier connectivity tool for remote login with the SSH protocol. \
It encrypts all traffic to eliminate eavesdropping, connection hijacking, and \
other attacks. In addition, OpenSSH provides a large suite of secure tunneling \
capabilities, several authentication methods, and sophisticated configuration options.

%description clients
OpenSSH is a free version of SSH (Secure SHell), a program for logging
into and executing commands on a remote machine. This package includes
the clients necessary to make encrypted connections to SSH servers.

%description server
OpenSSH is a free version of SSH (Secure SHell), a program for logging
into and executing commands on a remote machine. This package contains
the secure shell daemon (sshd). The sshd daemon allows SSH clients to
securely connect to your SSH server.

%description keycat
OpenSSH mls keycat is backend for using the authorized keys in the
openssh in the mls mode.

%description askpass
OpenSSH is a free version of SSH (Secure SHell), a program for logging
into and executing commands on a remote machine. This package contains
an X11 passphrase dialog for OpenSSH.

%description -n pam_ssh_agent_auth
Provides PAM module for the use of authentication with ssh-agent. Through the use of the\
forwarding of ssh-agent connection it also allows to authenticate with remote ssh-agent \
instance. The module is most useful for su and sudo service stacks.

%package_help

%prep
%setup -q -a 3
find %{_sourcedir} -type f -exec dos2unix -q {} \;
find %{_builddir}/%{name}-%{openssh_version} -type f -exec dos2unix -q {} \;

pushd pam_ssh_agent_auth-pam_ssh_agent_auth-0.10.4
%patch -P 4 -p2 -b .psaa-build
%patch -P 5 -p2 -b .psaa-seteuid
%patch -P 6 -p2 -b .psaa-visibility
%patch -P 8 -p2 -b .psaa-compat
%patch -P 7 -p2 -b .psaa-agent
%patch -P 9 -p2 -b .psaa-deref
%patch -P 10 -p2 -b .rsasha2
%patch -P 11 -p1 -b .psaa-configure-c99
# Remove duplicate headers and library files
rm -f $(cat %{SOURCE4})
popd

%patch -P 12 -p1 -b .role-mls
%patch -P 13 -p1 -b .privsep-selinux
%patch -P 14 -p1 -b .keycat
%patch -P 15 -p1 -b .ip-opts
%patch -P 16 -p1 -b .ipv6man
%patch -P 17 -p1 -b .sigpipe
%patch -P 18 -p1 -b .x11
%patch -P 20 -p1 -b .progress
%patch -P 21 -p1 -b .grab-info
%patch -P 22 -p1
%patch -P 23 -p1 -b .log-usepam-no
%patch -P 24 -p1 -b .gsskex
%patch -P 25 -p1 -b .force_krb
%patch -P 27 -p1 -b .ccache_name
%patch -P 28 -p1 -b .k5login
%patch -P 29 -p1 -b .kuserok
%patch -P 30 -p1 -b .fromto-remote
%patch -P 31 -p1 -b .contexts
%patch -P 32 -p1 -b .log-in-chroot
%patch -P 33 -p1 -b .scp
%patch -P 26 -p1 -b .GSSAPIEnablek5users
%patch -P 34 -p1 -b .sshdt
%patch -P 35 -p1 -b .sftp-force-mode
%patch -P 36 -p1 -b .s390-dev
%patch -P 37 -p1 -b .x11max
%patch -P 38 -p1 -b .systemd
%patch -P 39 -p1 -b .refactor
%patch -P 40 -p1 -b .sandbox
%patch -P 41 -p1 -b .pkcs11-uri
%patch -P 42 -p1 -b .scp-ipv6
%patch -P 43 -p1 -b .crypto-policies
%patch -P 44 -p1 -b .openssl-evp
%patch -P 45 -p1 -b .openssl-kdf
%patch -P 46 -p1 -b .visibility
%patch -P 47 -p1 -b .x11-ipv6
%patch -P 48 -p1 -b .keygen-strip-doseol
%patch -P 49 -p1 -b .preserve-pam-errors
%patch -P 50 -p1 -b .kill-scp
%patch -P 51 -p1 -b .scp-sftpdirs
%patch -P 52 -p1 -b .minrsabits
%patch -P 53 -p1 -b .ibmca
%patch -P 1 -p1 -b .audit
%patch -P 2 -p1 -b .audit-race
%patch -P 3 -p1 -b .audit-log
%patch -P 19 -p1 -b .fips
%patch -P 54 -p1 -b .ssh-manpage
%patch -P 55 -p1 -b .negotiate-supported-algs
%patch -P 0 -p1 -b .coverity

%patch -P 56 -p1
%patch -P 57 -p1
%patch -P 58 -p1
%patch -P 59 -p1
%patch -P 60 -p1
%patch -P 61 -p1
%patch -P 62 -p1
%patch -P 63 -p1
%patch -P 64 -p1
%patch -P 65 -p1
%patch -P 66 -p1
%patch -P 67 -p1
%patch -P 68 -p1
%patch -P 69 -p1
%patch -P 70 -p1
%patch -P 71 -p1
%patch -P 72 -p1
%patch -P 73 -p1
%patch -P 74 -p1
%patch -P 75 -p1

autoreconf
pushd pam_ssh_agent_auth-pam_ssh_agent_auth-0.10.4
autoreconf
popd

%build
CFLAGS="$RPM_OPT_FLAGS -fvisibility=hidden"; export CFLAGS

CFLAGS="$CFLAGS -Os"
%ifarch s390 s390x sparc sparcv9 sparc64
CFLAGS="$CFLAGS -fPIC"
%else
CFLAGS="$CFLAGS -fpic"
%endif
SAVE_LDFLAGS="$LDFLAGS"
LDFLAGS="$LDFLAGS -pie -z relro -z now"

export CFLAGS
export LDFLAGS

if test -r /etc/profile.d/krb5-devel.sh ; then
    source /etc/profile.d/krb5-devel.sh
fi
krb5_prefix=`krb5-config --prefix`
if test "$krb5_prefix" != "%{_prefix}" ; then
    CPPFLAGS="$CPPFLAGS -I${krb5_prefix}/include -I${krb5_prefix}/include/gssapi"; export CPPFLAGS
    CFLAGS="$CFLAGS -I${krb5_prefix}/include -I${krb5_prefix}/include/gssapi"
    LDFLAGS="$LDFLAGS -L${krb5_prefix}/%{_lib}"; export LDFLAGS
else
    krb5_prefix=
    CPPFLAGS="-I%{_includedir}/gssapi"; export CPPFLAGS
    CFLAGS="$CFLAGS -I%{_includedir}/gssapi"
fi

%configure \
    --sysconfdir=%{_sysconfdir}/ssh --libexecdir=%{_libexecdir}/openssh \
    --datadir=%{_datadir}/openssh --with-default-path=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin \
    --with-superuser-path=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin \
    --with-privsep-path=%{_var}/empty/sshd --disable-strip \
    --without-zlib-version-check --with-ssl-engine --with-ipaddr-display \
    --with-pie=no --without-hardening --with-systemd --with-default-pkcs11-provider=yes \
    --with-pam --with-selinux --with-audit=linux --with-security-key-buildin=yes \
%ifnarch riscv64 loongarch64 sw_64
     --with-sandbox=seccomp_filter \
%endif
    --with-kerberos5${krb5_prefix:+=${krb5_prefix}} --with-libedit

make
gtk2=yes

pushd contrib
if [ $gtk2 = yes ] ; then
    CFLAGS="$CFLAGS %{?__global_ldflags}" \
        make gnome-ssh-askpass2
    mv gnome-ssh-askpass2 gnome-ssh-askpass
else
    CFLAGS="$CFLAGS %{?__global_ldflags}"
        make gnome-ssh-askpass1
    mv gnome-ssh-askpass1 gnome-ssh-askpass
fi
popd

pushd pam_ssh_agent_auth-pam_ssh_agent_auth-0.10.4
LDFLAGS="$SAVE_LDFLAGS"
%configure --with-selinux --libexecdir=/%{_libdir}/security --with-mantype=man \
    --without-openssl-header-check
make
popd

%check
if [ -e /sys/fs/selinux/enforce ]; then 
    # Store the SElinux state
    cat /sys/fs/selinux/enforce > selinux.tmp
    setenforce 0
fi
make tests
if [ -e /sys/fs/selinux/enforce ]; then
    # Restore the SElinux state
    cat selinux.tmp > /sys/fs/selinux/enforce
    rm -rf selinux.tmp
fi

%install
mkdir -p -m755 $RPM_BUILD_ROOT%{_sysconfdir}/ssh
mkdir -p -m755 $RPM_BUILD_ROOT%{_sysconfdir}/ssh/ssh_config.d
mkdir -p -m755 $RPM_BUILD_ROOT%{_libexecdir}/openssh
mkdir -p -m755 $RPM_BUILD_ROOT%{_var}/empty/sshd
mkdir -p -m755 $RPM_BUILD_ROOT%{_sysconfdir}/bash_completion.d

%make_install

install -d $RPM_BUILD_ROOT/etc/pam.d/
install -d $RPM_BUILD_ROOT/etc/sysconfig/
install -d $RPM_BUILD_ROOT%{_libexecdir}/openssh
install -m644 %{SOURCE2} $RPM_BUILD_ROOT/etc/pam.d/sshd
install -m644 %{SOURCE5} $RPM_BUILD_ROOT/etc/pam.d/ssh-keycat
install -m644 %{SOURCE6} $RPM_BUILD_ROOT/etc/sysconfig/sshd
install -d -m755 $RPM_BUILD_ROOT/%{_unitdir}
install -m644 %{SOURCE7} $RPM_BUILD_ROOT/%{_unitdir}/sshd@.service
install -m644 %{SOURCE8} $RPM_BUILD_ROOT/%{_unitdir}/sshd.socket
install -m644 %{SOURCE9} $RPM_BUILD_ROOT/%{_unitdir}/sshd.service
install -m644 %{SOURCE10} $RPM_BUILD_ROOT/%{_unitdir}/sshd-keygen@.service
install -m644 %{SOURCE13} $RPM_BUILD_ROOT/%{_unitdir}/sshd-keygen.target
install -d -m755 $RPM_BUILD_ROOT/%{_userunitdir}
install -m644 %{SOURCE14} $RPM_BUILD_ROOT/%{_userunitdir}/ssh-agent.service
install -m644 %{SOURCE15} $RPM_BUILD_ROOT/%{_userunitdir}/ssh-agent.socket
install -m744 %{SOURCE11} $RPM_BUILD_ROOT/%{_libexecdir}/openssh/sshd-keygen
install -m755 contrib/ssh-copy-id $RPM_BUILD_ROOT%{_bindir}/
install contrib/ssh-copy-id.1 $RPM_BUILD_ROOT%{_mandir}/man1/
install -m644 -D %{SOURCE12} $RPM_BUILD_ROOT%{_tmpfilesdir}/%{name}.conf
install contrib/gnome-ssh-askpass $RPM_BUILD_ROOT%{_libexecdir}/openssh/gnome-ssh-askpass
install -m644 %{SOURCE16} $RPM_BUILD_ROOT/etc/bash_completion.d/ssh-keygen-bash-completion.sh
install -m744 %{SOURCE17} $RPM_BUILD_ROOT/%{_libexecdir}/openssh/ssh-host-keys-migration.sh
install -m644 %{SOURCE18} $RPM_BUILD_ROOT/%{_unitdir}/ssh-host-keys-migration.service
install -d $RPM_BUILD_ROOT/%{_localstatedir}/lib
touch $RPM_BUILD_ROOT/%{_localstatedir}/lib/.ssh-host-keys-migration

ln -s gnome-ssh-askpass $RPM_BUILD_ROOT%{_libexecdir}/openssh/ssh-askpass
install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/
install -m 755 contrib/redhat/gnome-ssh-askpass.csh $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/
install -m 755 contrib/redhat/gnome-ssh-askpass.sh $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/

perl -pi -e "s|$RPM_BUILD_ROOT||g" $RPM_BUILD_ROOT%{_mandir}/man*/*

pushd pam_ssh_agent_auth-pam_ssh_agent_auth-0.10.4
make install DESTDIR=$RPM_BUILD_ROOT
popd

%pre
getent group ssh_keys >/dev/null || groupadd -r ssh_keys || :

%pre server
getent group sshd >/dev/null || groupadd -g %{sshd_uid} -r sshd || :
getent passwd sshd >/dev/null || \
  useradd -c "Privilege-separated SSH" -u %{sshd_uid} -g sshd \
  -s /sbin/nologin -r -d /var/empty/sshd sshd 2> /dev/null || :

%post server
if [ $1 -gt 1 ]; then
    # In the case of an upgrade (never true on OSTree systems) run the migration
    # script to remove group ownership for host keys.
    %{_libexecdir}/openssh/ssh-host-keys-migration.sh
    # Prevent the systemd unit that performs the same service (useful for
    # OSTree systems) from running.
    touch /var/lib/.ssh-host-keys-migration
fi
%systemd_post sshd.service sshd.socket

%preun server
%systemd_preun sshd.service sshd.socket

%postun server
%systemd_postun_with_restart sshd.service

%post clients
%systemd_user_post ssh-agent.service
%systemd_user_post ssh-agent.socket

%preun clients
%systemd_user_preun ssh-agent.service
%systemd_user_preun ssh-agent.socket

%files
%license LICENCE
%doc CREDITS README.platform
%attr(0755,root,root) %dir %{_sysconfdir}/ssh
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/ssh/moduli
%attr(0755,root,root) %{_bindir}/ssh-keygen
%attr(0755,root,root) %dir %{_libexecdir}/openssh
%attr(2555,root,ssh_keys) %{_libexecdir}/openssh/ssh-keysign
%attr(0644,root,root) %{_sysconfdir}/bash_completion.d/ssh-keygen-bash-completion.sh

%files clients
%attr(0755,root,root) %{_bindir}/ssh
%attr(0755,root,root) %{_bindir}/scp
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/ssh/ssh_config
%attr(0755,root,root) %{_bindir}/ssh-agent
%attr(0755,root,root) %{_bindir}/ssh-add
%attr(0755,root,root) %{_bindir}/ssh-keyscan
%attr(0755,root,root) %{_bindir}/sftp
%attr(0755,root,root) %{_bindir}/ssh-copy-id
%attr(0755,root,root) %{_libexecdir}/openssh/ssh-pkcs11-helper
%attr(0755,root,root) %{_libexecdir}/openssh/ssh-sk-helper
%attr(0644,root,root) %{_userunitdir}/ssh-agent.service
%attr(0644,root,root) %{_userunitdir}/ssh-agent.socket

%files server
%dir %attr(0711,root,root) %{_var}/empty/sshd
%attr(0755,root,root) %{_sbindir}/sshd
%attr(0755,root,root) %{_libexecdir}/openssh/sftp-server
%attr(0755,root,root) %{_libexecdir}/openssh/sshd-keygen
%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/ssh/sshd_config
%attr(0644,root,root) %config(noreplace) /etc/pam.d/sshd
%attr(0640,root,root) %config(noreplace) /etc/sysconfig/sshd
%attr(0644,root,root) %{_unitdir}/sshd.service
%attr(0644,root,root) %{_unitdir}/sshd@.service
%attr(0644,root,root) %{_unitdir}/sshd.socket
%attr(0644,root,root) %{_unitdir}/sshd-keygen@.service
%attr(0644,root,root) %{_unitdir}/sshd-keygen.target
%attr(0644,root,root) %{_tmpfilesdir}/openssh.conf
%attr(0644,root,root) %{_unitdir}/ssh-host-keys-migration.service
%attr(0744,root,root) %{_libexecdir}/openssh/ssh-host-keys-migration.sh
%ghost %attr(0644,root,root) %{_localstatedir}/lib/.ssh-host-keys-migration

%files keycat
%attr(0755,root,root) %{_libexecdir}/openssh/ssh-keycat
%attr(0644,root,root) %config(noreplace) /etc/pam.d/ssh-keycat

%files askpass
%attr(0644,root,root) %{_sysconfdir}/profile.d/gnome-ssh-askpass.*
%attr(0755,root,root) %{_libexecdir}/openssh/gnome-ssh-askpass
%attr(0755,root,root) %{_libexecdir}/openssh/ssh-askpass

%files -n pam_ssh_agent_auth
%license pam_ssh_agent_auth-pam_ssh_agent_auth-0.10.4/OPENSSH_LICENSE
%attr(0755,root,root) %{_libdir}/security/pam_ssh_agent_auth.so
%attr(0644,root,root) %{_mandir}/man8/pam_ssh_agent_auth.8*

%files help
%doc ChangeLog OVERVIEW PROTOCOL* README README.privsep README.tun README.dns TODO
%doc HOWTO.ssh-keycat
%attr(0644,root,root) %{_mandir}/man1/scp.1*
%attr(0644,root,root) %{_mandir}/man1/ssh*.1*
%attr(0644,root,root) %{_mandir}/man1/sftp.1*
%attr(0644,root,root) %{_mandir}/man5/ssh*.5*
%attr(0644,root,root) %{_mandir}/man5/moduli.5*
%attr(0644,root,root) %{_mandir}/man8/ssh*.8*
%attr(0644,root,root) %{_mandir}/man8/sftp-server.8*

%changelog
* Tue Feb 18 2025 bitianyuan <bitianyuan@huawei.com> - 9.6p1-5
- Type:CVE
- CVE:CVE-2025-26465 CVE-2025-26466
- SUG:NA
- DESC:Fix CVE-2025-26465 CVE-2025-26466

* Fri Feb 7 2025 bitianyuan <bitianyuan@huawei.com> - 9.6p1-4
- Type:CVE
- CVE:CVE-2024-39894
- SUG:NA
- DESC:Fix CVE-2024-39894

* Mon Dec 23 2024 bitianyuan <bitianyuan@huawei.com> - 9.6p1-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:dos2unix all file

* Fri Dec 20 2024 bitianyuan <bitianyuan@huawei.com> - 9.6p1-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:change '/usr/bin/bash' to '/bin/bash'

* Mon Dec 09 2024 yanglu <yanglu72@h-partners.com> - 9.6p1-1
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:update openssh version to 9.6p1

* Tue Oct 29 2024 bitianyuan <bitianyuan@huawei.com> - 9.3p2-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport some upstream patches

* Fri Jul 12 2024 renmingshuai <renmingshuai@huawei.com> - 9.3p2-5
- Type:CVE
- CVE:CVE-2023-51384
- SUG:NA
- DESC:Fix CVE-2023-51384

* Tue Jul 2 2024 renmingshuai <renmingshuai@huawei.com> - 9.3p2-4
- Type:CVE
- CVE:CVE-2024-6387
- SUG:NA
- DESC:Fix CVE-2024-6387

* Mon Apr 29 2024 renmingshuai <renmingshuai@huawei.com> - 9.3p2-3
- Type:bugfix
- CVE:
- SUG:NA
- DESC:Disable SElinux when make tests

* Wed Jan 31 2024 renmingshuai<renmingshuai@huawei.com> - 9.3p2-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:move pam_ssh_agent_auth man page to sub-package

* Wed Jan 24 2024 renmingshuai<renmingshuai@huawei.com> - 9.3p2-1
- Type:update
- CVE:NA
- SUG:NA
- DESC:update to 9.3p2

* Tue Dec 26 2023 renmingshuai<renmingshuai@huawei.com> - 9.3p1-3
- Type:CVE
- CVE:CVE-2023-48795,CVE-2023-51385
- SUG:NA
- DESC:fix CVE-2023-48795 and CVE-2023-51385

* Fri Aug 25 2023 renmingshuai<renmingshuai@huawei.com> - 9.3p1-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:use correct ssh-agent.socket name

* Thu Jul 27 2023 renmingshuai<renmingshuai@huawei.com> - 9.3p1-1
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:update to 9.3p1

* Tue Jun 13 2023 renmingshuai<renmingshuai@huawei.com> - 9.1p1-6
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix misspelling

* Sat May 27 2023 renmingshuai<renmingshuai@huawei.com> - 9.1p1-5
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix environment variable

* Sat Mar 18 2023 renmingshuai<renmingshuai@huawei.com> - 9.1p1-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:backport some upstreams patches and delete unused patches

* Tue Feb 28 2023 renmingshuai<renmingshuai@huawei.com> - 9.1p1-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:set default ssh_config

* Mon Feb 06 2023 renmingshuai<renmingshuai@huawei.com> - 9.1p1-2
- Type:CVE
- CVE:CVE-2023-25136
- SUG:NA
- DESC:fix CVE-2023-25136

* Mon Jan 30 2023 renmingshuai<renmingshuai@huawei.com> - 9.1p1-1
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:update to openssh-9.1p1

* Mon Jan 9 2023 renmingshuai <renmingshuai@huawei.com> - 8.8p1-17
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix possible NULL deref when built without FIDO

* Tue Jan 3 2023 renmingshuai <renmingshuai@huawei.com> - 8.8p1-16
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix test failure and always make tests

* Thu Dec 29 2022 renmingshuai <renmingshuai@huawei.com> - 8.8p1-15
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:avoid integer overflow of auth attempts

* Thu Dec 29 2022 renmingshuai <renmingshuai@huawei.com> - 8.8p1-14
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:PubkeyAcceptedKeyTypes has been renamed to PubkeyAcceptedAlgorithms in openssh-8.5p1

* Thu Dec 29 2022 renmingshuai <renmingshuai@huawei.com> - 8.8p1-13
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:add strict scp check for CVE-2020-15778

* Thu Dec 29 2022 renmingshuai <renmingshuai@huawei.com> - 8.8p1-12
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:backport some upstream patches

* Thu Dec 29 2022 renmingshuai <renmingshuai@huawei.com> - 8.8p1-11
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:add sw_64 

* Fri Dec 16 2022 renmingshuai <renmingshuai@huawei.com> - 8.8p1-10
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:Fix ssh-keygen -Y check novalidate requires name

* Mon Nov 28 2022 zhaozhen <zhaozhen@loongson.cn> - 8.8p1-9
- Type:feature
- CVE:NA
- SUG:NA
- DESC:Add loongarch64 support

* Mon Nov 28 2022 renmingshuai<renmingshuai@huawei.com> - 8.8p1-8
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:add better debugging

* Wed Nov 2 2022 renmingshuai<renmingshuai@huawei.com> - 8.8p1-7
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:add ssh-keygen bash completion

* Thu Sep 01 2022 duyiwei<duyiwei@kylinos.cn> - 8.8P1-6
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:enable "include /etc/ssh/sshd_config.d/*.config" again

* Fri Jul 29 2022 kircher<majun65@huawei.com> - 8.8p1-5
- Type:bugfix
- CVE:Na
- SUG:NA
- DESC:add SMx support in openssh

* Thu May 05 2022 seuzw<930zhaowei@163.com> - 8.8p1-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix incorrect sftp-server binary path in /etc/ssh/sshd_config

* Wed Mar 09 2022 duyiwei<duyiwei@kylinos.cn> - 8.8P1-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:enable "include /etc/ssh/sshd_config.d/*.config"

* Mon Mar 07 2022 kircher<majun65@huawei.com> - 8.8P1-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:add sshd.tmpfiles

* Thu Oct 28 2021 kircher<kircherlike@outlook.com> - 8.8P1-1
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:update to openssh-8.8p1

* Fri Oct 8 2021 renmingshuai<renmingshuai@hauwei.com> - 8.2P1-15
- Type:cves
- CVE:CVE-2021-41617
- SUG:NA
- DESC:fix CVE-2021-41617

* Sat Sep 18 2021 kircher<kircherlike@outlook.com> - 8.2P1-14
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:backport patch from github to fix NULL ref

* Fri Jul 30 2021 kircher<majun65@huawei.com> - 8.2P1-13
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:remove debug message from sigchld handler

* Tue Jul 20 2021 seuzw<930zhaowei@163.com> - 8.2P1-12
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:move closefrom to before first malloc

* Fri Jul 09 2021 panchenbo<panchenbo@uniontech.com> - 8.2P1-11
- fix pam_ssh_agent_auth.8.gz conflicts

* Thu May 20 2021 seuzw<930zhaowei@163.com> - 8.2P1-10
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:add strict-scp-check for check command injection

* Mon Jan 4 2021 chxssg<chxssg@qq.com> - 8.2P1-9
- Type:cves
- CVE:CVE-2020-14145
- SUG:NA
- DESC:fix CVE-2020-14145

* Wed Nov 18 2020 gaihuiying<gaihuiying1@huawei.com> - 8.2P1-8
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:adjust pam_ssh_agent_auth release number

* Tue Nov 17 2020 gaihuiying<gaihuiying1@huawei.com> - 8.2P1-7
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:keep pam_ssh_agent_auth change release number with openssh

* Tue Sep 15 2020 liulong<liulong20@huawei.com> - 8.2P1-6
- Type:cves
- ID:CVE-2018-15919
- SUG:NA
- DESC:Fix CVE-2018-15919

* Thu Jul 2 2020 zhouyihang<zhouyihang3@huawei.com> - 8.2P1-5
- Type:cves
- ID:CVE-2020-12062
- SUG:NA
- DESC:Fix CVE-2020-12062

* Tue Jun 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 8.2P1-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add requires for openssh-server in openssh

* Wed May 6 2020 openEuler Buildteam <buildteam@openeuler.org> - 8.2P1-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix update problem

* Sat Apr 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 8.2P1-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix pre problem

* Thu Apr 16 2020 openEuler Buildteam <buildteam@openeuler.org> - 8.2P1-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update to 8.2P1

* Mon Mar 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.8P1-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:move sshd.service in %post server

* Wed Mar 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.8P1-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:reduction of authority

* Fri Mar 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.8P1-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:separate package

* Thu Mar 5 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.8P1-9
- Type:cves
- ID:CVE-2018-15919
- SUG:NA
- DESC:Fix CVE-2018-15919

* Thu Mar 5 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.8P1-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:debug3 to verbose in command line

* Tue Jan 21 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.8P1-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add the patch for bugfix

* Mon Dec 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 7.8P1-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:delete the patch

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 7.8P1-5
- Type:cves
- ID:NA
- SUG:restart
- DESC:fix cves

* Fri Sep 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 7.8p1-4
- Package init
